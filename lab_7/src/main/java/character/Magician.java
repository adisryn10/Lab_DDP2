package character;

//  write Magician Class here
public class Magician extends Human{
	public Magician(String nama,int hp){
		super(nama,hp);
	}
	//membuat method burn(Khusus Magician)
	public String burn(Player Enemy){
		String infoburn = "";
		//jika masih hidup
		if(this.getStatus()){
			//cek jika musuh magician(damage x 2)
            if (Enemy instanceof Magician){
                Enemy.setHp(Enemy.getHp() - 2*10);
            }
            //jika musuh human atau monster
            else{
                Enemy.setHp(Enemy.getHp() - 10);
            }
            infoburn = "Nyawa "+ Enemy.getName() + " " + Enemy.getHp();
            //cek jika Hp musuh kurang dari 0
            if (Enemy.getHp() <= 0){
                infoburn += "\n dan matang";
                Enemy.setBurn(true);
                Enemy.setStatus(false);
            }
        }
        //jika sudah mati
        else{
            infoburn = nama + "tidak bisa membakar " + Enemy.getName();
        }
        return infoburn;
	}

	//method canEat
	public boolean canEat(Player Enemy){
		if (Enemy.getStatus()){
			return false;
		}
		else if(Enemy instanceof Human){
			return false;
		}
		else if (Enemy.getIsBurn() && Enemy instanceof Monster){
			return true;
		}
		return true;
	}
}