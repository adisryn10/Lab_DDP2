package character;

//  write Human Class here
public class Human extends Player{
	public Human(String nama,int hp){
		super(nama,hp,"Human");
	}
	//method can eat Human
	public boolean canEat(Player Enemy){
		if (Enemy.getIsBurn() && Enemy instanceof Monster){
			return true;
		}
		return false;
	}
}