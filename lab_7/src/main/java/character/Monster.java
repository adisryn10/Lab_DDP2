package character;

//  write Monster Class here
public class Monster extends Player{
	private String roar;
	public Monster(String nama,int hp){
		super(nama,2*hp,"Monster");
		this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
	}
	//method canEat Monster
	public boolean canEat(Player Enemy){
		if (!Enemy.getStatus()){
			return true;
		}
		return false;
	}
	//membuat method roar
	public String roar(){
		return roar;
	}
	//membuat method setter roar
	public void setRoar(String roar){
		this.roar = roar;
	}
}