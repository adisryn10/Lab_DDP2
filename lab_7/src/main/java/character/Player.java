package character;

public class Player{
	//class constructor
	protected String nama;
	protected int hp;
	protected String listDiet;
	protected String jenis;
	protected boolean isBurn;
	protected boolean status;

	public Player(String nama,int hp,String jenis){
		this.nama = nama;
		this.hp = hp;
		this.listDiet = "";
		this.isBurn = false;		//semua karakter pada mulanya belum terbakar
		if (hp <= 0){
			this.status = false;	//jika hp 0 karakter mati
		}
		else{
			this.status = true;
		}
		this.jenis = jenis;
	}
	//membuat setter dan getter
	public void setName(String nama){
		this.nama = nama;
	}
	public String getName(){
		return nama;
	}
	public void setHp(int hp){
		if (hp < 0){
			this.hp = 0;
		}
		else{
			this.hp = hp;
		}
	}
	public int getHp(){
		return hp;
	}
	public void setDiet(String name, String jenis){
		this.listDiet += jenis + " " + name;
	}
	public String getDiet(){
		return listDiet;
	}
	public boolean getIsBurn(){
		return isBurn;
	}
	public void setBurn(boolean burn){
		this.isBurn = burn;
	}
	public boolean getStatus(){
		return status;
	}
	public void setStatus(boolean status){
		this.status = status;
	}
	public String getJenis(){
		return jenis;
	}
	//membuat method attack (berlaku semua player)
	public String attack(Player Enemy){
		String infoattack = "";
		//jika karakter masi hidup(dapat menyerang)
		if(status){
			//cek apakah musuh merupakan Magician
            if (Enemy instanceof Magician){
                Enemy.setHp(Enemy.getHp() - 2*10);
            }
            //jika musuh Human atau Monster
            else{
                Enemy.setHp(Enemy.getHp() - 10);
            }

            infoattack = "Nyawa "+ Enemy.getName() + " " + Enemy.getHp();
            //cek jika Hp kurang dari 0, maka karakter mati
            if (Enemy.getHp() <= 0){
                Enemy.setStatus(false);
            }
        }
        //jika karakter sudah mati
        else{
            infoattack = nama + "tidak bisa menyerang " + Enemy.getName();
        }
        return infoattack;
    }
    //membuat method eat
    public String eat(Player Enemy){
    	String infoeat = "";
        this.hp += 15;
        setDiet(Enemy.getName(), Enemy.getJenis());
        Enemy.setStatus(false);
        infoeat = nama + " memakan " + Enemy.getName();
        infoeat += "\nNyawa " + nama + " kini " + this.hp;
        return infoeat;
    }
    //membuat method status
    public String status(){
    	String infoStatus = "";
    	infoStatus += jenis + " " + nama + "\n";
        infoStatus += "HP : " + hp + "\n";
        if (status){
            infoStatus += "Masih hidup\n";
        }
        else{														//jika karakter sudah mati
            infoStatus += "Sudah meninggal dunia dengan damai\n";
        }
        if (listDiet.equals("")){									//jika karakter belum makan apa-apa
            infoStatus += "Belum memakan siapa siapa";
        }
        else{
            infoStatus += "Memakan "+ listDiet;
        }
        return infoStatus;
    }
    //membuat mehtod canEat (akan di override dikelas masing-masing)
    //agar tiap kelas fokus ke kelasnya masing-masing
    public boolean canEat(Player Enemy){
    	return false;
    }

}
