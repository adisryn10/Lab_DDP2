import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> arrayListPlayer = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player pemain : arrayListPlayer){
            if (pemain.getName().equals(name)){
                return pemain;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        String infoadd = "";
        //check jika sudah ada karakter yang bernama sama
        if (find(chara) != null){
             infoadd =  "Sudah ada karakter bernama " + chara;
        }
        //jika nama belum ada (berhasil ditambahkan)
        else{
            if (tipe == "Human"){
                Human karakter = new Human(chara,hp);
                arrayListPlayer.add(karakter);
            }
            else if (tipe == "Monster"){
                Monster karakter = new Monster(chara, hp);
                arrayListPlayer.add(karakter);

            }
            else if (tipe == "Magician"){
                Magician karakter = new Magician(chara,hp);
                arrayListPlayer.add(karakter);
            }
            infoadd = chara + " ditambah ke game" ;
        }
        return infoadd;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        String infoadd = "";
        //cek jika sudah ada karakter bernama sama
        if (find(chara) != null){
             infoadd =  "Sudah ada karakter bernama " + chara;
        }
        //jika nama belum ada (berhasil ditemukan)
        //khusus untuk monster
        else{
            if (tipe == "Monster"){
                Monster karakter = new Monster(chara, hp);
                karakter.setRoar(roar);
                arrayListPlayer.add(karakter);
                infoadd = chara + " ditambahkan ke game" ;
            }
        }

        return infoadd;
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        String inforemove = "";
        //jika nama yang ingin diremove tidak ada
        if (find(chara) == null){
             inforemove =  "Tidak ada " + chara;
        }
        //nama yang diremove ada (berhasil diremove)
        else{
            Player pemain = find(chara);
            arrayListPlayer.remove(pemain);
            inforemove= chara + " dihapus dari game";
        }

        return inforemove;
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        String infoStatus = "";
        //jika nama yang dicari tidak ada
        if (find(chara) == null){
             infoStatus =  "Tidak ada " + chara;
        }
        //jika nama yang dicari ada(berhasil)
        else{
            Player pemain = find(chara);
            infoStatus = pemain.status();
        }
        return infoStatus;

    
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String infoStatus = "";
        for (int i = 0; i < arrayListPlayer.size() ; i++){
            Player pemain = arrayListPlayer.get(i);
            infoStatus += pemain.status();
            if (i != arrayListPlayer.size()-1){
                infoStatus +="\n";
            }
        }
        return infoStatus;        
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        String infodiet = "";
        //jika nama yang dicari tidak ada di list
        if (find(chara) == null){
            infodiet =  "Tidak ada " + chara;
        }
        //jika nama yang dicari ada 
        else{
            Player Home = find(chara);
            infodiet = Home.getDiet();
        }
        return infodiet;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String infoDietAll = "";
        for (Player pemain : arrayListPlayer){
            infoDietAll += pemain.getDiet();
        }
        return infoDietAll;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        String infoattack = "";
        //jika meName tidak ada
        if (find(meName) == null){
            infoattack =  "Tidak ada " + meName;
        }
        //jika enemyName tidak ada 
        else if (find(enemyName) == null){
            infoattack =  "Tidak ada " + enemyName;
        }
        //nama berhasil ditemukan(ada 2-2nya)
        else{
            Player Home = find(meName);
            Player Enemy = find(enemyName);
            infoattack = Home.attack(Enemy);
        }
        return infoattack;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        String infoburn = "";
        //jika meName tidak ada
        if (find(meName) == null){
            infoburn =  "Tidak ada " + meName;
        }
        //jika enemyName tidak ada
        else if (find(enemyName) == null){
            infoburn =  "Tidak ada " + enemyName;
        }
        //jika keduanya ada
        else{
            Player Home = find(meName);
            Player Enemy = find(enemyName);
            //cek apakah home merupakan magician
            if (Home instanceof Magician){
                infoburn = ((Magician)Home).burn(Enemy); 
            }
            //jika bukan magician
            else{
                infoburn = meName + " tidak bisa membakar " + enemyName;
            }
        }
        return infoburn;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        String infoeat = "";
        //jika meName tidak ad di arraylist
        if (find(meName) == null){
            infoeat =  "Tidak ada " + meName;
        }
        //jika enemyName tidak ada di arraylist
        else if (find(enemyName) == null){
            infoeat =  "Tidak ada " + enemyName;
        }
        //keduanya ada
        else{
            Player Home = find(meName);
            Player Enemy = find(enemyName);
            //cek apakah home dapat memakan Enemy (syarat dari tiap home berbeda-beda)
            if (Home.canEat(Enemy)){
                infoeat = Home.eat(Enemy);
                arrayListPlayer.remove(Enemy);
            }
            //jika home tidak dapat memakan enemy
            else{
                infoeat = meName + " tidak bisa memakan " + enemyName;
            }
        }

        return infoeat;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        String inforoar = "";
        //jika nama tidak ada di arraylist
        if (find(meName) == null){
            inforoar =  "Tidak ada " + meName;
        }
        //jika nama ada di arraylist
        else{
            Player Home = find(meName);
            //cek apakah Home merupakan Monster(roar hanya khusus monster)
            if (Home instanceof Monster){
                inforoar = ((Monster)Home).roar();
            }
            //jika home bukan monster
            else{
                inforoar = meName + " tidak bisa berteriak ";
            }
        }
            
    return inforoar;
    }
}