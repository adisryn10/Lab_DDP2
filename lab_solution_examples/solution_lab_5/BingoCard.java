import java.lang.StringBuilder;

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; // array pembantu untuk cek status angka
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		String output = "";
		if(numberStates[num]!=null){
			if(!numberStates[num].isChecked()){
				numberStates[num].setChecked(true);
				checkBingo(num);
				output = num + " tersilang";
			}
			else{
				output = num + " sebelumnya sudah tersilang";
			}
		}
		else{
			output = "Kartu tidak memiliki angka " + num;
		}
		return output;
	}
	
	private void checkBingo(int n){
		Number start = numberStates[n];
		int i = start.getX();
		int j = start.getY();
		
		//cek secara horizontal
		int horizontal = checkBingoHelper(i,j,"left") + checkBingoHelper(i,j,"right");
		
		//cek secara vertikalal
		int vertikal = checkBingoHelper(i,j,"up") + checkBingoHelper(i,j,"down");
		
		//cek secara diagonal kiri atas ke kanan bawah
		int diagonal1 = checkBingoHelper(i,j,"upleft") + checkBingoHelper(i,j,"downright");
		
		//cek secara diagonal kiri bawah ke kanan atas
		int diagonal2 = checkBingoHelper(i,j,"downleft") + checkBingoHelper(i,j,"upright");
		
		if(horizontal == 6 || vertikal == 6 || diagonal1 == 6 || diagonal2 == 6){
			this.setBingo(true);
		}
	}
	
	private int checkBingoHelper(int row, int col, String direction){
		if(row<0 || row>4){
			return 0;
		}
		if(col<0 || col>4){
			return 0;
		}
		if(!numbers[row][col].isChecked()){
			return 0;
		}
		int x = 0;
		int y = 0;
		
		switch(direction.toLowerCase()){
		case "left":
			x = -1;
			break;
		case "right":
			x = 1;
			break;
		case "up":
			y = -1;
			break;
		case "down":
			y = 1;
			break;
		case "upleft":
			x = -1;
			y = -1;
			break;
		case "downright":
			x = 1;
			y = 1;
			break;
		case "upright":
			x = 1;
			y = -1;
			break;
		case "downleft":
			x = -1;
			y = 1;
			break;
		}
		
		return 1 + checkBingoHelper(row+x,col+y,direction);
	}
	
	public String info(){
		StringBuilder str = new StringBuilder();
		for(int i=0; i<5; i++){
			str.append("| ");
			for(int j=0; j<5; j++){
				if(!numbers[i][j].isChecked()){
					str.append(numbers[i][j].getValue());
				}
				else{
					str.append("X ");
				}
				str.append(" | ");
			}
			str.deleteCharAt(str.length()-1);
			str.append("\n");
		}
		str.deleteCharAt(str.length()-1);
		return str.toString();
	}
	
	public void restart(){
		for(int i=0; i<5; i++){
			for(int j=0; j<5; j++){
				numbers[i][j].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}	

}
