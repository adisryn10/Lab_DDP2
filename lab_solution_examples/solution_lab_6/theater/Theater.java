package theater;

import movie.Movie;
import ticket.Ticket;

import java.util.ArrayList;

public class Theater {
    private ArrayList<Ticket> tickets;
    private int cashBalance;
    public static int totalRevenueEarned = 0;
    private String locationName;
    private Movie[] movies;


    public Theater(String locationName, int cashBalance, ArrayList<Ticket> initialTickets, Movie[] movies) {
        this.tickets = new ArrayList<>();
        this.cashBalance = cashBalance;
        totalRevenueEarned += cashBalance;
        this.locationName = locationName;
        this.tickets = initialTickets;
        this.movies = movies;
    }

    public void refillTicket(Ticket newTicket) {
        this.tickets.add(newTicket);
    }

    public int getCashBalance() {
        return cashBalance;
    }

    public void setCashBalance(int cashBalance) {
        this.cashBalance = cashBalance;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    // Beli tiket standar
    public Ticket purchaseTicket(Ticket ticket) {
        int ticketFare = ticket.getPrice();
        cashBalance += ticketFare;
        totalRevenueEarned += ticketFare;
        tickets.remove(ticket);
        return ticket;


    }

    public Ticket findTicket(String movieName, String schedule, String threeD) {
        boolean is3D = (threeD.equalsIgnoreCase("3 Dimensi")) ? true : false;
        for (Ticket ticket : tickets) {
            if (ticket.getFilm().getTitle().equalsIgnoreCase(movieName) &&
                    ticket.getSchedule().equals(schedule) && is3D == ticket.is3D()) {
                return ticket;
            }
        }
        return null;
    }

    public Movie checkMovieDetail(String movieName) {
        for (Movie movie : movies) {
            if (movie.getTitle().equalsIgnoreCase(movieName)) {
                return movie;
            }
        }
        return null;
    }

    //BONUS
    public boolean hasMovie(Movie film) {
        for (Movie movie : movies) {
            if (movie.equals(film)) {
                return true;
            }
        }
        return false;
    }

    public void printInfo() {
        String cetakFilm = "";
        for (int i = 0; i < movies.length; i++) {
            cetakFilm += movies[i].getTitle();
            if (i < movies.length - 1) {
                cetakFilm += ", ";
            }
        }
        String print = "------------------------------------------------------------------" +
                "\nBioskop\t\t\t\t\t: " + this.locationName +
                "\nSaldo Kas\t\t\t\t: " + this.cashBalance +
                "\nJumlah tiket tersedia\t: " + this.tickets.size() +
                "\nDaftar Film tersedia\t: " + cetakFilm +
                "\n------------------------------------------------------------------";
        System.out.println(print);
    }

    public static void printTotalRevenueEarned(Theater[] theaters) {
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalRevenueEarned);
        System.out.println("------------------------------------------------------------------");
        for (Theater t : theaters) {
            System.out.println("Bioskop\t\t: " + t.getLocationName());
            System.out.println("Saldo Kas\t: Rp. " + t.getCashBalance() + "\n");
        }
        System.out.println("------------------------------------------------------------------");
    }
}