import java.util.Scanner;

/* 
Code author by	: I Made Adisurya Nugraha
NPM				: 1706984625
Class			: D
Gitlab id		: adisryn10
*/


public class RabbitHouse{
	static int total = 0;
	static int normal(String kata){
		if (kata.length() == 1){			//base case
			return 1;
		}
		else{
			int result = 0;
			String potongankata = kata.substring(1,kata.length());
			result = 1 + (kata.length()*normal(potongankata)) ;	// recursive case
			return result;
		}	
	}
	static boolean ispalindrome(String word){
		if (word.length() <= 1){			//base case
			return true;
		}
		else{
			int panjang = word.length();
			String hurufawal = word.substring(0,1);
			String hurufakhir = word.substring(panjang-1,panjang);
			if (hurufawal.equals(hurufakhir)){
				return ispalindrome(word.substring(1,panjang-1)) ;
			}
			else{
				return false;
			}
		}
	}
	static int palindrome(String word){
		if (ispalindrome(word)){			//basecase
			return 0; 
		}
		else{
			total += 1;
			for (int i = 0;i < word.length(); i++){
				
				String newstr = word.substring(0, i) + word.substring(i + 1);
				palindrome(newstr);			//recursive case
			}
			return total;
		}
	}
	public static void main (String[] args){
		// Membuat input scanner
		Scanner input = new Scanner(System.in);
		
		// Meminta input
		String kata = input.nextLine();
		
		// Memisahkan kata
		String[] katasplit = kata.split(" ");
		String perintah = katasplit[0];
		String param = katasplit[1];
		
		//Menjalankan fungsi
		if (perintah.equals("normal")){
			int perin = normal(param);
			System.out.println(perin);
		}
		else if (perintah.equals("palindrome")){
			int hasilbonus = palindrome(param);
			System.out.println(hasilbonus);
		}
	}
}