/*
This code is created by:
Author	: I Made Adisurya Nugraha
Class	: D
NPM		: 1706984625
Gitlab	: adisryn10
*/

public class Manusia{
	//Menginisiasi objek kelas
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;
	
	//Menerapkan overloading pada method Manusia
	public Manusia (String nama,int umur,int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
	}
	public Manusia(String nama, int umur){
		this.nama = nama;
		this.umur = umur;
		this.uang = 50000;
		this.kebahagiaan = 50;
	}
	
	//Menerapkan setters dan getters
	//Membuat method set nama dan get nama
	public void setNama(String nama){
		this.nama = nama;
	}
	public String getNama(){
		return nama;
	}
	
	//Membuat method set umur dan get umur
	public void setUmur(int umur){
		this.umur = umur;
	}
	public int getUmur(){
		return umur;
	}
	
	//Membuat method set uang dan get uang
	public void setUang(int uang){
		this.uang = uang;
	}
	public int getUang(){
		return uang;
	}
	
	//Membuat method set kebahagiaan dan get kebahagiaan
	public void setKebahagiaan(float kebahagiaan){
		if (kebahagiaan > 100){					//case kebahagiaan lebih dari maximum
			this.kebahagiaan = 100;
		}
		else if (kebahagiaan < 0){				//case kebahagiaan kurang dari minimum
			this.kebahagiaan = 0;
		}
		else{
			this.kebahagiaan = kebahagiaan;		//normal case
		}
	}
	
	public float getKebahagiaan(){
		return kebahagiaan;
	}
	
	//Overloading pada method beriUang(dengan param uang atau tidak)
	//Membuat method memberi uang
	public void beriUang(Manusia penerima){
		
		//Menghitung uang yang akan diberi
		int total = 0;
		char[] chname = penerima.getNama().toCharArray();
		for (char c : chname){
			total += (int)c;
		}
		total = total * 100;
		
		//Jika uang yang diberi kurang dari uang yang dimiliki (normal case)
		if (total < getUang()){
			penerima.setUang (penerima.getUang()+ total);
			penerima.setKebahagiaan (penerima.getKebahagiaan() + total/6000f);
			setKebahagiaan (getKebahagiaan()+ total/6000f);
			setUang (getUang()- total);	
			System.out.println(nama + " memberi uang sebanyak " + total + " kepada " + penerima.nama+ " mereka berdua senang :D" );
		}
		
		//Tidak dapat memberi uang karena uang yang dimiliki kurang
		else{
			System.out.println(nama + " ingin memberi uang kepada "+ penerima.nama+ " namun tidak memiliki cukup uang :'(");
		}	
	}
	
	//Method memberi uang dengan parameter tambahan yaitu uang yang diberi
	public void beriUang(Manusia penerima, int beri){
		
		//Jika uang yang diberi kurang dari uang yang dimiliki (normal case)
		if (beri < getUang()){
			penerima.setUang (penerima.getUang()+ beri);
			penerima.setKebahagiaan (penerima.getKebahagiaan() + beri/6000f);
			setKebahagiaan (getKebahagiaan()+ beri/6000f);
			setUang (getUang()- beri);
			System.out.println(nama + " memberi uang sebanyak " + beri + " kepada " + penerima.nama + " mereka berdua senang :D" );
		}
		
		//Tidak dapat memberi uang karena uang yang dimiliki kurang
		else{
			System.out.println(nama + " ingin memberi uang kepada "+ penerima.nama + " namun tidak memiliki cukup uang :'(");
		}
	}
	
	// Membuat method bekerja
	public void bekerja(int durasi, int bebankerja){
		
		//Jika umur kurang dari 18 (tidak dapat bekerja)
		if (getUmur() < 18){
			System.out.println(nama+ " belum boleh belajar karena masih dibawah umur D:");
		}
		
		//Dapat bekerja
		else{
			int BebanKerjaTotal = durasi * bebankerja;
			//Case jika beban kerja kurang dari kebahagiaan (normal case)
			if (BebanKerjaTotal <= getKebahagiaan()){
				setKebahagiaan (getKebahagiaan()- BebanKerjaTotal);
				int pendapatan = BebanKerjaTotal*10000;
				System.out.println(nama + " bekerja full time, total pendapatan : "+ pendapatan);
				setUang (getUang() + pendapatan);
			}
			//Jika beban kerja lebih dari kebahagiaan
			else{
				int DurasiBaru = (int)kebahagiaan/bebankerja;
				BebanKerjaTotal = DurasiBaru * bebankerja;
				int pendapatan = BebanKerjaTotal*10000;
				setKebahagiaan(getKebahagiaan()- BebanKerjaTotal) ;
				System.out.println(nama+" tidak dapat bekerja secara full time karena sudah terlalu lelah, total pendapatan "+ pendapatan);
				setUang (getUang() + pendapatan);
			}
		}
	}
	//Membuat method rekreasi
	public void rekreasi(String namatempat){
		int panjangString = namatempat.length();
		int Biaya = panjangString*10000;
		
		//Jika uang yang dimiliki cukup(normal case)
		if (getUang() > Biaya){
			setKebahagiaan(getKebahagiaan() + panjangString);
			setUang(getUang() - Biaya);
			System.out.println(nama + " berekreasi di "+ namatempat+", "+nama+" senang :)");
		}
		//Jika uang tidak cukup
		else{
			System.out.println(nama+" tidak mempunyai cukup uang untuk berekreasi di "+namatempat + " :(");
		}
	}
	//Membuat method sakit
	public void sakit(String namapenyakit){
		int panjangString = namapenyakit.length();
		setKebahagiaan(getKebahagiaan() - panjangString);
		System.out.println(nama + " terkena penyakit "+ namapenyakit+" :0");
	}
	
	//Membuat method cetak/println
	public String toString(){
		return  "Nama\t\t:" + getNama()+"\n"+
				"Umur\t\t:" + getUmur()+"\n"+
				"Uang\t\t:" + getUang()+"\n"+
				"Kebahagiaan\t:" + getKebahagiaan();
	}
}	