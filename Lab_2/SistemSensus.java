import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author I Made Adisurya Nugraha, NPM 1706984625 , Kelas D, GitLab Account: adisryn10
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);

		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
				
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		String panjangs = input.nextLine();
		double panjang = Double.parseDouble(panjangs);
		if ((panjang < 0) || (panjang > 250)){
			System.out.println("WARNING : Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Lebar Tubuh (cm)       : ");					
		String lebars = input.nextLine();
		double lebar = Double.parseDouble(lebars);
		if ((lebar < 0) || (lebar > 250)){
			System.out.println("WARNING : Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		} 
		System.out.print("Tinggi Tubuh (cm)      : ");
		String tinggis = input.nextLine();
		double tinggi = Double.parseDouble(tinggis);
		if ((tinggi < 0) || (tinggi > 250)){
			System.out.println("WARNING : Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Berat Tubuh (kg)       : ");
		String berats = input.nextLine();
		double berat = Double.parseDouble(berats);
		if ((berat < 0) || (berat > 150)){
			System.out.println("WARNING : Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		String makanans = input.nextLine();
		int makanan = Integer.parseInt(makanans);
		if ((makanan < 0) || (makanan > 20)){
			System.out.println("WARNING : Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		String jumlahCetakans = input.nextLine();
		int jumlahCetakan = Integer.parseInt(jumlahCetakans);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		double  rasio = berat / ((panjang/100) * (lebar/100) * (tinggi/100));

		for (int i = 1;i <= jumlahCetakan; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase
			String cetak;
			
			// TODO Periksa ada catatan atau tidak
			if (catatan.isEmpty()){
				cetak = "Tidak ada catatan tambahan";
				} 
			else{
				cetak = "Catatan : " + catatan;
				};

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK " + penerima + "\n---------------------";
			System.out.println(hasil);
			System.out.println(nama + "-" + alamat);
			System.out.println("Lahir pada tanggal " + tanggalLahir);
			System.out.println("Rasio Berat Per Volume = " + (int)rasio + " kg/m^3");
			System.out.println(cetak);
			}
		
		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		System.out.println("\nREKOMENDASI APARTEMEN \n"+
						   "----------------------\n");
		
		
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		String namaSlice = nama.substring(0,1);
		int totalchar = 0;
		int vol = (int)panjang*(int)lebar*(int)tinggi;
		char[] ch = nama.toCharArray();
		for (char c : ch){
			totalchar += (int)c;
		}
		totalchar = (vol + totalchar)%10000;
		String nomorSlice = String.valueOf(totalchar);
		
		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = namaSlice + nomorSlice;
	
		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000*365*makanan;

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String tahunLahir = tanggalLahir.substring(6,10); // lihat hint jika bingung
		int tahunLahirSplit = Integer.parseInt(tahunLahir);
		int umur = 2018 - tahunLahirSplit;
		String rekomendasi = "";
		String namaApart = "";
		String kabupaten = "";
		

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		if (0 < umur && umur <= 18){
			namaApart = "PPMT";
			kabupaten = "Rotunda";
		
		}
		else if ((0 < anggaran && anggaran < 100000000) && (19 < umur && umur < 1018)){
			namaApart = "Teksas";
			kabupaten = "Sastra";
			
		}
		else if ((anggaran > 100000000) && (19 < umur && umur < 1018)){
			namaApart = "Mares";
			kabupaten = "Margonda";
			
		}
		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		rekomendasi = "Mengetahui : Identitas keluarga : " + nama + "-" + nomorKeluarga + "\n"+
					  "Menimbang  : Anggaran makanan tahunan : " + "Rp " + anggaran+"\n"+
					  "             Umur kepala keluarga     : " + umur + " tahun\n"+
					  "Memutuskan : Keluarga " + nama + " akan ditempatkan di :\n"+
					  namaApart + ", kabupaten " + kabupaten; 
		System.out.print(rekomendasi);
							 
		input.close();
		
	}
}
