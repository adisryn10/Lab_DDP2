/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */
 
import java.util.Scanner;

public class BingoCard {
	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}
	
	//Membuat method marknum
	public String markNum(int num){
				
		//Jika angka yang diinput tidak ada dalam Array
		if (numberStates[num] == null){								
			return ("Kartu tidak memiliki angka "+ num);
		}
		
		
		//Jika angka yang diinput sudah disilang sebelumnya
		else if (numberStates[num].isChecked() == true){	
			return (num + " sudah tersilang sebelumnya");
		}
		
		//Kartu belum tersilang
		else{
			numberStates[num].setChecked(true);
			checkbingo();								//check apakah sudah bingo
			if (isBingo()){								//jika sudah bingo
				System.out.println(info());
				System.exit(0);
			}
			return num + " tersilang";		
			
			}		
		}
	

	
	//Membuat method info
	public String info(){
		String hasil = "";
		for (int i = 0; i < 5; i++){
			hasil += ("| ");
			for (int j = 0; j < 5; j++){
				if (numbers[i][j].isChecked()==true){
					hasil +=(" X |");
				}
				else{
					hasil +=(numbers[i][j].getValue() + " |"  );
				}
				if (j !=4){
					hasil += " ";
				}
			}
			if (i != 4){
				hasil += ("\n");
		}
		}
		return hasil;
	}
	
	//Membuat method restart
	public String restart(){
		for (int i = 0; i < 5; i++){
			for (int j = 0; j < 5; j++){
				numbers[i][j].setChecked(false);
			}
		}
		return ("Mulligan!");
	}
	
	//Membuat method check bingo
	public void checkbingo(){
		
		//Cek Bingo Horizontal
		for (Number[] elem : numbers){
			if (elem[0].isChecked() == true && elem[1].isChecked() == true && elem[3].isChecked() == true &&
			    elem[2].isChecked() == true && elem[4].isChecked() == true){
				System.out.println("BINGO!");
				setBingo(true);
			}
		}
		
		//Cek Bingo Vertikal
		for (int i = 0; i < 5; i++){
			int cek = 0;
			for (int j = 0; j < 5; j++){
				if (numbers[j][i].isChecked())
					cek ++;
			}
			if (cek == 5){
				System.out.println("BINGO!");
				setBingo(true);
				break;
			}
		}
		
		//Cek Bingo Diagonal dari kiri atas ke kanan bawah
		int cekdiagonal = 0;
		for (int i = 0; i < 5; i++){
			if (numbers[i][i].isChecked()){
				cekdiagonal ++;
			}
		}
		
		if(cekdiagonal == 5){
			System.out.println("BINGO!");
			setBingo(true);	
				
		}
		
		//Cek Bingo Diagonal dari kiri bawah ke kanan atas
		int cekdiagonal2 = 0;
		for (int i = 0; i < 5; i++){
			if (numbers[i][4-i].isChecked()){
				cekdiagonal2 ++;
			}
		}
		
		if(cekdiagonal2 == 5){
			System.out.println("BINGO!");
			setBingo(true);	
				
		}
	}
}


















