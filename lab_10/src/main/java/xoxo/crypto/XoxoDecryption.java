package xoxo.crypto;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */

    public String decrypt(String encryptedMessage, int seed) {
        if (seed < 0 || seed > 36){
            throw new RangeExceededException("seed must between 0 and 36");
        }
        if (encryptedMessage.length() > 1250){
            throw new SizeTooBigException("Size more than 10 kb");
        }
        //TODO: Implement decryption algorithm
        final int length = encryptedMessage.length();
        String decryptedMessage = "";
        for (int i = 0; i < length; i++) {
            //TODO: throw InvalidCharacterException for message requirements
            int a = this.hugKeyString.charAt(i  % this.hugKeyString.length());
            int b = a ^ seed;
            int c = b - 'a';
            int d = encryptedMessage.charAt(i) ^ c;
            decryptedMessage += (char)d;
        }
        return decryptedMessage; 
    }
}