package xoxo;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import xoxo.crypto.*;
import xoxo.exceptions.*;
import xoxo.key.*;
import xoxo.util.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author I Made Adisurya Nugraha
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     * to give action listener to button encrypt and decrypt
     */
    public void run() {
        ActionListener decryptAct = new ActionListener(){
            int num = 1;
            public void actionPerformed(ActionEvent e) {
                XoxoDecryption xd = new XoxoDecryption(gui.getKeyText());
                String seed = gui.getSeedText();
                String result = "";
                try{
                    int seedInt = Integer.parseInt(seed);
                    result =  xd.decrypt(gui.getMessageText(),seedInt);
                    gui.appendLog(result);
                    writeDrc(result,num); 
                    num ++;
                }
                catch(NumberFormatException nf){
                    if (seed.equalsIgnoreCase("DEFAULT_SEED")){
                        result =  xd.decrypt(gui.getMessageText(),18);
                        gui.appendLog(result);
                        writeDrc(result,num); 
                        num ++;
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Seed must be Integer or DEFAULT_SEED");
                    }
                }
            } 
        };
        this.gui.setDecryptFunction(decryptAct);

        ActionListener encryptAct = new ActionListener() {
            int num = 1;
            public void actionPerformed(ActionEvent e) {
                XoxoEncryption xe = new XoxoEncryption(gui.getKeyText());
                XoxoMessage xm;
                String seed = gui.getSeedText();
                try{
                    int seedInt = Integer.parseInt(seed);
                    xm =  xe.encrypt(gui.getMessageText(),seedInt); 
                    gui.appendLog(xm.getEncryptedMessage());
                    writeEnc(xm.getEncryptedMessage(),num); 
                    num++;
                }
                catch(NumberFormatException nf){
                    if (seed.equalsIgnoreCase("DEFAULT_SEED")){
                        xm =  xe.encrypt(gui.getMessageText());
                        gui.appendLog(xm.getEncryptedMessage());
                        writeEnc(xm.getEncryptedMessage(),num); 
                        num++;
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Seed must be Integer or DEFAULT_SEED");
                    }
                }
            } 
        };
        this.gui.setEncryptFunction(encryptAct);

        ActionListener clearLogAct = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                gui.clearLog();
            } 
        };
        this.gui.setClearFunction(clearLogAct);
    }
    /**
    * Method to write the result of encryption to enc format
    * @param message that going to write in the txt file type of string
    * @param number for the name file type of integer
    */
    public void writeEnc(String message, int num){
        try {
            File file = new File("C://Users//Adi Surya//Documents//Documents//I Made Adi//UI//SEM 2//DDP 2//Lab_DDP2//lab_10//src//main//java//encrypt "+ String.valueOf(num)+ ".enc");
            file.createNewFile();
            FileWriter writer = new FileWriter(file); 
            writer.write(message); 
            writer.flush();
            writer.close();
            }
        catch (IOException e) {
             e.printStackTrace();
        }
    }
    /**
    * Method to write the result of decryption to txt format
    * @param message that going to write in the txt file type of string
    * @param number for the name file type of integer
    */
    public void writeDrc(String message,int num){
        try {
            File file = new File("C://Users//Adi Surya//Documents//Documents//I Made Adi//UI//SEM 2//DDP 2//Lab_DDP2//lab_10//src//main//java//decrypt "+ String.valueOf(num)+ ".txt");
            file.createNewFile();
            FileWriter writer = new FileWriter(file); 
            writer.write(message); 
            writer.flush();
            writer.close();
            }
        catch (IOException e) {
             e.printStackTrace();
        }
    }
}