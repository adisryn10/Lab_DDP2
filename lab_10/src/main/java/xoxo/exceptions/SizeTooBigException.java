package xoxo.exceptions;
import javax.swing.*;

/**
 * An exception that is thrown if the size of message
 * is more than 100 kb
 */
public class SizeTooBigException extends RuntimeException{

    /**
     * Class constructor.
     */

    public SizeTooBigException(String message) {
    	super(message);
    	JOptionPane.showMessageDialog(null, message);
    }

    
}