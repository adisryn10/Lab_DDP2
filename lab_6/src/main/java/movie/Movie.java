//put the class to package movie
package movie;

//make Movie Class
public class Movie{
	
	private String judul;
	private String genre;
	private int durasi;
	private String rating;
	private String jenis;
	
	public Movie(String judul, String rating, int durasi, String genre, String jenis){
		this.judul = judul;
		this.genre = genre;
		this.durasi = durasi;
		this.rating = rating;
		this.jenis = jenis;
	}
	//applied getter to use in customer class
	public String getJudul(){
		return judul;
	}
	public String getGenre(){
		return genre;
	}
	public int getDurasi(){
		return durasi;
	}
	//first getRating in int (for check umur in customer)
	public int getIntRating(){
		if (rating == "Dewasa"){
			return 17;
		}
		else if (rating == "Remaja"){
			return 13;
		}
		else{
			return 0;
		}
	}
	//second getRating (for print orderticket in class customer)
	public String getRating(){
		return rating;
	}
	public String getJenis(){
		return jenis;
	}
	
	//Make method printInfo
	public void printInfo(){
		System.out.println("------------------------------------------------------------------");
		System.out.println(	"Judul\t: "	    	+ judul  + 
							"\nGenre\t: "   	+ genre  +
							"\nDurasi\t: "  	+ durasi + " menit"+
							"\nRating\t: "  	+ rating +
							"\nJenis\t: Film "	+ jenis  );
		System.out.println("------------------------------------------------------------------");
	}
}