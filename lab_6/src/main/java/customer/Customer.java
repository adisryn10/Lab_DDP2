//put customer class in customer package and import modul
package customer;
import ticket.Ticket;
import movie.Movie;
import theater.Theater;

//make customer class
public class Customer{
	private String name;
	private String jenisKelamin;
	private int umur;
	
	public Customer(String name,String jenisKelamin, int umur){
		this.name = name;
		this.jenisKelamin = jenisKelamin;
		this.umur = umur;
	}
	//make method order ticket
	public Ticket orderTicket(Theater teaters, String judul, String hari, String jenis){
		//found(as a method for else, will be break if the ticket is available, not available when the looping is finished)
		found :{
			//Looping the ticket 
			for (Ticket tiket : teaters.getTicket()){
				Movie filmCheck = tiket.getMovie();											//variable ticket
				//check if the hari, jenis, and ticket available
				if ((hari == tiket.getHari()) && (jenis == tiket.getJenis()) && (judul == filmCheck.getJudul())){
					
					//if available but umur isn't enough to order
					if (umur < tiket.getMovie().getIntRating()){
						System.out.println(name + " masih belum cukup umur untuk menonton " + filmCheck.getJudul() + " dengan rating "+ filmCheck.getRating());
						break found;
					}
					//success to order
					else{
						System.out.println(name+" telah membeli tiket " + filmCheck.getJudul() + " jenis "+ tiket.getJenis() + " di " + 
										   teaters.getName() + " pada hari " + hari + " seharga Rp. "+ tiket.getHarga());
						teaters.setSaldo(teaters.getSaldo() + tiket.getHarga());
						return tiket;
					}						
				}
			}
			//if the ticket isn't available
			System.out.println("Tiket untuk film "+ judul + " jenis "+ jenis +" dengan jadwal " + hari + " tidak tersedia di " + teaters.getName() );
		}
		return null;
	}
	//make a findMovie method
	public void findMovie(Theater teaters, String judul){
		Movie[] movieList = teaters.getMovie();
		//found (will be break if the movie is available)
		found : {
			for (Movie film : movieList){
				if (judul == film.getJudul()){
					film.printInfo();
					break found;
				}
			}
			//if the movie is not available
			System.out.println("Film " + judul +" yang dicari "+ name +" tidak ada di bioskop "+ teaters.getName());
		}	
	}
}