//put Ticket class in package ticket and import movie to use in class constructure
package ticket;
import movie.Movie;

//make a ticket class
public class Ticket{

	private String judul;
	private String hari;
	private boolean jenis;
	private Movie movies;
	
	public Ticket(Movie movies ,String hari, boolean jenis){
		this.movies = movies;
		this.hari = hari;
		this.jenis = jenis;
	}
	//applied getters to use in customer class
	public Movie getMovie(){
		return movies;
	}
	
	public String getHari(){
		return hari;
	}
	//getters if true equals with 3 Dimensi and false equals to Biasa
	public String getJenis(){
		if (jenis){
			return "3 Dimensi";
		}
		else{
			return "Biasa";
		}
	}
	//get jenis in boolean
	public boolean getBolJenis(){
		return jenis;
	}
	
	//getters harga
	public int getHarga(){
		if  (((hari == "Minggu") && jenis) || ((hari == "Sabtu") && jenis)){	//if sabtu minggu and 3 Dimensi
			return 100000* 120/100;
		}
		else if (hari == "Minggu" || hari == "Sabtu"){							//if sabtu minggu and Biasa
			return 100000;
		}
		else if (jenis){														//if senin-kamis and 3 Dimensi
			return 60000 * 120/100;
		}
		else{																	//if senin-kamis and Biasa
			return 60000;
		}
	}
	
	public void printInfo(){
		System.out.println("------------------------------------------------------------------");
		System.out.println(	"Film\t: "	    		+ judul + 
							"\nJadwal Tayang\t: "   + hari  +
							"\nJenis\t: "  			+ jenis);
		System.out.println("------------------------------------------------------------------");
	}	
	
}