//Put the class to package theater and import modul
package theater;
import java.util.*;
import ticket.Ticket;
import movie.Movie;

//Make theater class
public class Theater{
	
	private String nameTheater;
	private int saldo;
	private Movie[] movieTheater;
	private ArrayList<Ticket> ticketAwal;
	
	public Theater(String nameTheater, int saldo, ArrayList<Ticket> ticketAwal, Movie[] movieTheater){
		this.nameTheater = nameTheater;
		this.saldo = saldo;
		this.ticketAwal = ticketAwal;
		this.movieTheater = movieTheater;
	}
	//applied getters to use in customer class
	public Movie[] getMovie(){
		return movieTheater;
	}
	public String getName(){
		return nameTheater;
	}
	public ArrayList<Ticket> getTicket(){
		return ticketAwal;
	}
	public int getSaldo(){
		return saldo;
	}
	
	//applied setters to use in customer class
	public void setSaldo(int saldo){
		this.saldo = saldo;
	}

	//method print info
	public void printInfo(){
		//make printinfo as a format in lab instruction
		System.out.println("------------------------------------------------------------------");
		System.out.println(	"Bioskop\t\t\t: "	    		+ nameTheater 	+ 
							"\nSaldo Kas\t\t: "   			+ saldo     	+ 
							"\nJumlah Tiket Tersedia\t: "	+ ticketAwal.size());
		String printfilm = "";
		
		//to make coma until the last film
		int comaCount = 0;
		//looping to get Movie name for daftar film tersedia
		for (Movie films : movieTheater){
			printfilm += films.getJudul();
			comaCount += 1;
			if (comaCount != movieTheater.length){
				printfilm += ", ";
			}
		}				
		System.out.println("Daftar Film Tersedia\t: "	+ printfilm);
		System.out.println("------------------------------------------------------------------");
	}
	//make a printTotalRevenueEarned
	public static void printTotalRevenueEarned(Theater[] teaters){
		int totalUang = 0;
		String totalUangStr = "";
		String infoTheater = "";
		infoTheater += ("------------------------------------------------------------------");
		//Looping to get total Uang Koh Mas
		for (Theater i : teaters){ 
			infoTheater += ("\nBioskop\t\t: " 	 + String.valueOf(i.getName()) + 
							"\nSaldo Kas\t: Rp." + String.valueOf(i.getSaldo()) + "\n");
			totalUang += i.getSaldo();	
		}
		infoTheater += ("\n------------------------------------------------------------------");
		totalUangStr = ("Total uang yang dimiliki Koh Mas : Rp. "+ String.valueOf(totalUang));
		System.out.println(totalUangStr);
		System.out.println(infoTheater);
	}
}