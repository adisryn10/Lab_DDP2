package Employee;

//Membuat kelas intern
public class Intern extends Employee{
	public Intern(String name,int salary){
		super(name,salary);
	}
	//Method canRecruit pada Intern (Tidak bisa rekrut)
	public boolean canRecruit(Employee bawahan){
		return false;
	}
}