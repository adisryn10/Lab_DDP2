package Employee;

//Membuat kelas Manager
public class Manager extends Employee{
	public Manager(String name,int salary){
		super(name,salary);
	}
	//Method canRecruit pada Intern (Bisa merekrut selama bawahan dibawah 10)
	public boolean canRecruit(Employee bawahan){
		if (getBawahan().size() < 10){
			return true;
		}
		return false;
	}
}