package Employee;

//Membuat kelas staff
public class Staff extends Employee{
	public Staff(String name,int salary){
		super(name,salary);
	}
	//Method canRecruit pada Intern (Hanya bisa jika bawahan intern dan bawahan kurang dari 1o)
	public boolean canRecruit(Employee bawahan){
		if (getBawahan().size() >= 10){
			return false;
		}
		else if (bawahan instanceof Manager){
			return false;
		}
		else{
			return true;
		}
	}
}