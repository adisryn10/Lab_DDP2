//put class employee in package employee
package Employee;
//import modul
import java.util.ArrayList;

public class Employee{
	//class constructor
	protected String name;
	protected int salaryPokok;
	protected int salaryNow;
	protected ArrayList<Employee> bawahan = new ArrayList<Employee>();
	protected int jumlahGajian;
	protected int batasGaji;

	public Employee(String name, int salary){
		this.name = name;
		this.salaryPokok = salary;
		this.salaryNow = salary;
		this.jumlahGajian = 0 ;
		this.batasGaji = 18000;
	}
	//Membuat setter dan getter dari tiap constructor
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	//Set dan get gaji untuk mengecek dan set gaji saat ini
	public void setSalaryPokok(int salary){
		this.salaryPokok = salary;
	}
	public int getSalaryPokok(){
		return salaryPokok;
	}
	//Method tambah gaji
	public void tambahGaji(){
		this.salaryNow += salaryPokok;
	}
	public int getSalaryNow(){
		return salaryNow;
	}
	//Set dan get jumlah gajian (Berisi counter)
	public void jumlahGajian(){
		this.jumlahGajian += 1;
	}
	public int getJumlahGajian(){
		return jumlahGajian;
	}
	//Set dan get batas gaji (berlaku untuk staff sebagai parameter naik pangkat)
	public void setBatasGaji(int gaji){
		this.batasGaji = gaji;
	}
	public int getBatasGaji(){
		return batasGaji;
	}
	//Set dan get menambah bawahan dan set bawahan(Setelah naik pangkat)
	public void tambahBawahan(Employee bawahan){
		this.bawahan.add(bawahan);
	}
	public void setBawahan(ArrayList<Employee> listPekerja){
		this.bawahan = listPekerja;
	}
	public ArrayList<Employee> getBawahan(){
		return bawahan;
	}
	//Method canRecrut yang defaultnya false dan kemudian akan di override
	public boolean canRecruit(Employee bawahan){
		return false;
	}
	//Method cek bawahan (Untuk cek apakah bawahan sudah terdaftar)
	public boolean cekPekerja(String name){
		for (Employee pekerja : this.bawahan){
			if (pekerja.getName().equals(name)){
				return true;
			}
		}
		return false;
	}
}