import Employee.*;
import java.util.ArrayList;

public class Corporate{
    //Make arrayList of Employee
	ArrayList<Employee> arrayListEmployee = new ArrayList<Employee>();

    //Membuat method find pekerja
	public Employee find(String name){
        for (Employee pekerja : arrayListEmployee){
            if (pekerja.getName().equals(name)){
                return pekerja;
            }
        }
        return null;
    }
    //membuat method tambah karyawan
    public void tambahKaryawan(String name,String tipe,int salary){
        //Cek jika nama sudah terdaftar
    	if (find(name) != null){
    		System.out.println("Karyawan dengan nama " + name + " telah terdaftar");
    	}
        //Jika belum terdaftar
    	else{
            //cek tiap tipe
    		if (tipe.equals("MANAGER")){
    			Manager pekerja = new Manager(name,salary);
    			arrayListEmployee.add(pekerja);
    		}
    		else if (tipe.equals("STAFF")){
    			Staff pekerja = new Staff(name,salary);
    			arrayListEmployee.add(pekerja);
    		}
    		else if (tipe.equals("INTERN")){
    			Intern pekerja = new Intern(name,salary);
    			arrayListEmployee.add(pekerja);
    		}
    		System.out.println(name + " mulai bekerja sebagai " + tipe + " di PT. TAMPAN");
    	}
    }
    //membuat method cetak status employee
    public void cetakStatus(String name){
        //cek jika belum terdaftar
    	if (find(name) == null){
    		System.out.println("Karyawan tidak ditemukan");
    	}
        //jika sudah terdaftar
    	else{
    		Employee pekerja = find(name);
    		System.out.println(name + " " + pekerja.getSalaryPokok());
    	}
    }
    //membuat method rekrut
    public void recruit(String merekrut, String direkrut){
        //cek jika nama perekrut / direkrut tidak ada
    	if (find(merekrut) == null){
    		System.out.println("Nama tidak berhasil ditemukan");
    	}
    	else if (find(direkrut) == null){
    		System.out.println("Nama tidak berhasil ditemukan");
    	}
        //Jika keduanya ada
    	else{
    		Employee recruiting = find(merekrut);
    		Employee recruted = find(direkrut);
            //Jika yang direkrut sudah menjadi bawahan
    		if (recruiting.cekPekerja(direkrut)){
    			System.out.println("Karyawan " + direkrut + " telah menjadi bawahan " + merekrut);
    		}
            //Jika bawahan berhasil ditambahkan
    		else if (recruiting.canRecruit(recruted)){
    			recruiting.tambahBawahan(recruted);
    			System.out.println("Karyawan " + direkrut + " berhasil ditambahkan menjadi bawahan " + merekrut);
    		}
            //Jika bawahan tidak berhasil ditambahkan
    		else{
    			System.out.println("Anda tidak layak memiliki bawahan");
    		}
    	}
    }
    //Membuat method gajian
    public void gajian(){
    	System.out.println("Semua karyawan telah diberikan gaji");
    	for (Employee pekerja : arrayListEmployee){
    		pekerja.jumlahGajian();   //Menambah frekuensi gajian
    		pekerja.tambahGaji();     //Menambah gaji para pekerja
            //Cek apabila sudah 6 kali gajian
    		if (pekerja.getJumlahGajian() % 6 == 0){
    			int gajiBefore = pekerja.getSalaryPokok();
    			int gajiAfter = pekerja.getSalaryPokok() * 110/100;
    			pekerja.setSalaryPokok(gajiAfter);
    			System.out.println(pekerja.getName() + " mengalami kenaikan gaji sebesar 10% dari " + gajiBefore+ " menjadi " + gajiAfter);
    		}
            //Cek apabila gaji staff sudah melebihi batas gaji staff
    		if (pekerja instanceof Staff && pekerja.getSalaryNow() > pekerja.getBatasGaji()){
    			Manager pekerjaNow = new Manager(pekerja.getName(),pekerja.getSalaryPokok());
    			pekerjaNow.setBawahan(pekerja.getBawahan());
    			arrayListEmployee.remove(pekerja);
    			arrayListEmployee.add(pekerjaNow);
    			System.out.println("Selamat, " + pekerjaNow.getName() + " telah dipromosikan menjadi MANAGER");
    		}
    	}
    }
    //Membuat method setGaji untuk staff (Default)
    public void setBatasGaji(int gaji){
        for(Employee pekerja : arrayListEmployee){
            pekerja.setBatasGaji(gaji);
        }
    }
}