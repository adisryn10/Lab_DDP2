//Mengimport module
import java.util.Scanner;
import Employee.*;

//Membuat kelas main
class Lab8 {
    public static void main(String[] args) {
    	Corporate corporate = new Corporate();
        //Membuat input
    	Scanner input = new Scanner (System.in);
    	while (true){
    		String kalimat = input.nextLine();
    		String[] splitKalimat = kalimat.split(" ");
            //Jika input berupa angka
            if (splitKalimat.length == 1 && !splitKalimat[0].equals("GAJIAN")){
                try{
                    int gaji = Integer.parseInt(splitKalimat[0]);
                    corporate.setBatasGaji(gaji);
                }
                catch(Exception gaji){
                    System.out.println("Perintah salah");
                }
            }
            //Jika input tambah karyawan
    		else if (splitKalimat[0].equals("TAMBAH_KARYAWAN")){
    			String tipe = splitKalimat[1];
    			String name = splitKalimat[2];
    			int salary = Integer.parseInt(splitKalimat[3]);
    			corporate.tambahKaryawan(name,tipe,salary);
    		}
            //Jika input status
    		else if (splitKalimat[0].equals("STATUS")){
    			String name = splitKalimat[1];
    			corporate.cetakStatus(name);
    		}
            //Jika input tambah bawahan
    		else if (splitKalimat[0].equals("TAMBAH_BAWAHAN")){
    			String penambah = splitKalimat[1];
    			String ditambah = splitKalimat[2];
    			corporate.recruit(penambah,ditambah);
    		}
            //Jika input gajian
    		else if (splitKalimat[0].equals("GAJIAN")){
    			corporate.gajian();
    		}
            //Selain perintah yang tersedia
            else{
                System.out.println("Perintah salah");
            }
    	}
    }
}