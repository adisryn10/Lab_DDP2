package lab9;

import lab9.user.User;
import lab9.event.Event;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
* Class representing event managing system
*/
public class EventSystem{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem(){
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
    * Method to search object User based on string name
    * @return Object type of User, null if the name doesn't registered
    * @param name of the user type of String
    */
    
    public User getUser(String name){
         for (User person : users){
            if (person.getName().equals(name)){
                return person;
            }
        }
        return null;
    }

    /**
    * Method to search object Event based on string name
    * @return Object type of Event, null if the name doesn't registered
    * @param name of the Event type of String
    */

    public Event findEvent(String name){
        for (Event acara : events){
            if (acara.getName().equals(name)){
                return acara;
            }
        }
        return null;
    }

    /**
    * Method to add event into arrayList of Event. This only succeed if parameter has the correct condition (start < end)
    * @return Object type of String, which tell the condition of this method, either is success or no
    * @param name of the user type of String
    * @param date start of the event, type of String
    * @param date end of the event, type of String
    * @param cost of the event per hour, type of String
    */

    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr){
        String addEventStr = "";

        String[] splitAwal = startTimeStr.split("_");
        String[] tanggalAwalSplit = splitAwal[0].split("-");
        String[] waktuAwalSplit = splitAwal[1].split(":");

        String[] splitAkhir = endTimeStr.split("_");
        String[] tanggalAkhirSplit = splitAkhir[0].split("-");
        String[] waktuAkhirSplit = splitAkhir[1].split(":");

        int tahunAwal = Integer.parseInt(tanggalAwalSplit[0]);
        int tahunAkhir = Integer.parseInt(tanggalAkhirSplit[0]);

        int bulanAwal = Integer.parseInt(tanggalAwalSplit[1]);
        int bulanAkhir = Integer.parseInt(tanggalAkhirSplit[1]);

        int hariAwal = Integer.parseInt(tanggalAwalSplit[2]);
        int hariAkhir = Integer.parseInt(tanggalAkhirSplit[2]);

        int jamAwal = Integer.parseInt(waktuAwalSplit[0]);
        int jamAkhir = Integer.parseInt(waktuAkhirSplit[0]);

        int menitAwal = Integer.parseInt(waktuAwalSplit[1]);
        int menitAkhir = Integer.parseInt(waktuAkhirSplit[1]);

        int detikAwal = Integer.parseInt(waktuAwalSplit[2]);
        int detikAkhir = Integer.parseInt(waktuAkhirSplit[2]);

        Date startDate = new Date(tahunAwal-1900,bulanAwal-1,hariAwal,jamAwal,menitAwal,detikAwal);
        Date endDate = new Date(tahunAkhir-1900,bulanAkhir-1,hariAkhir,jamAkhir,menitAkhir,detikAkhir);

        if (findEvent(name) != null){
            addEventStr = "Event " + name + " sudah ada!";
        }
        else if (startDate.before(endDate)){
            Event eventAdd = new Event(name,startDate,endDate,costPerHourStr);
            events.add(eventAdd);
            addEventStr = "Event " + name +" berhasil ditambahkan!";
        }
        else{
            addEventStr = "Waktu yang diinputkan tidak valid!";
        }
        return addEventStr;
    }
    
    /**
    * Method to add user into arrayList of user. This only succeed if name parameter doesnt registered before
    * @return Object type of String, which tell the condition of this method, either is success or no
    * @param name of the user with String Type
    */
    public String addUser(String name){
        String addUserStr = "";
        if (getUser(name) != null){
            addUserStr = "User "+ name + " sudah ada!";
        }
        else{
            User userAdd = new User(name);
            users.add(userAdd);
            addUserStr = "User "+ name + " berhasil ditambahkan!";
        }
        return addUserStr;
    }

    /**
    * Accessor to get The Event based on string parameter
    * @return Object type of String, Which tell the information about the event
    * @param name of the event with String Type
    */
    public String getEvent(String eventName){
        Event acara = findEvent(eventName);
        return acara.toString();
    }

    /**
    * Method to add register an event into the user's list of Event. It only succed if there is no overlap between 
    * the event that will register and the user's list of event
    * @return Object type of String, which tell the condition of this method, either is success or no
    * @param name of the user with String Type 
    * @param name of the event with String Type
    */
    public String registerToEvent(String userName, String eventName){
        String registerStr = "";
        if (getUser(userName) == null && findEvent(eventName) == null){
            registerStr = "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName;
        }
        else if (getUser(userName) == null){
             registerStr = "Tidak ada pengguna dengan nama " + userName;
        }
        else if (findEvent(eventName) == null){
            registerStr = "Tidak ada acara dengan nama " + eventName;
        }
        else{
            User person = getUser(userName);
            Event acara = findEvent(eventName);

            if (person.addEvent(acara)){
                registerStr = userName + " berencana menghadiri " + eventName + "!";
            }
            else{
                registerStr = userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
            }
        }
        return registerStr;
    }
}