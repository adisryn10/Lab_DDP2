package lab9.event;
import java.math.BigInteger;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
* A class representing an event and its properties
*/

public class Event implements Comparable<Event> {
    /** Name of event */
    private String name;
    
    /** The beginning time of event */
    private Date startDate;

    /** The end time of event */
    private Date endDate;
    
    /** Cost of the event */
    private String costPerHourStr;
    
    /**
    * Constructor
    * Initializes an event object with given name, Date start, Date End and String cost
    * @param name of the user type of String
    * @param date start of the event, type of String
    * @param date end of the event, type of String
    * @param cost of the event per hour, type of Strings
    */
    public Event(String name, Date startDate, Date endDate, String costPerHourStr){
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.costPerHourStr = costPerHourStr;
    }
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName(){
        return this.name;
    }

    /**
    * Accessor for cost field. 
    * @return cost of this event instance
    */
    public BigInteger getCost(){
        long totalTime = this.endDate.getTime() - this.startDate.getTime();
        long totalHour = totalTime/3600000;
        BigInteger hour = new BigInteger(String.valueOf(totalHour));
        BigInteger cost = new BigInteger(costPerHourStr);
        BigInteger finalCost = cost.multiply(hour);
        return finalCost;
    }
    /**
    * Accessor for startDate field. 
    * @return Date start of this event instance
    */
    public Date getStartDate(){
        return this.startDate;
    }

    /**
    * Accessor for endDate field. 
    * @return Date end of this event instance
    */
    public Date getEndDate(){
        return this.endDate;
    }
    
    /** 
    *Method to print the information about the event.
    *@return the information (Name, Date Start, Date End and cost) of the event
    */
    public String toString(){
        String infoEvent = "";

        SimpleDateFormat startString = new SimpleDateFormat("dd-MM-YYYY, HH:mm:ss");
        String startTimeStr = startString.format(startDate);

        SimpleDateFormat endString = new SimpleDateFormat("dd-MM-YYYY, HH:mm:ss");
        String endTimeStr = endString.format(endDate);

        infoEvent += name +
                     "\nWaktu mulai: " + startTimeStr + 
                     "\nWaktu selesai: " + endTimeStr + 
                     "\nBiaya kehadiran: " + getCost();
        return infoEvent;
    }

    /**
    *Method to check if the event overLaps with another event
    *@return boolean true if the event overLap and false if the event doesn't overLap
    *@param object type of Event
    */
    public boolean overLap(Event acara){
        if (acara.getStartDate().after(this.startDate) && acara.getStartDate().before(this.endDate)){
            return true;
        }
        else if (acara.getEndDate().after(this.startDate) && acara.getEndDate().before(this.endDate)){
            return true;
        }
        else{
            return false;
        }
    }

    /**
    *Method to sort the Event based on the time to start the event
    *@return int 1 if this date before compareDate and -1 if this date after compareDate
    *@param object type of Event
    */
    public int compareTo(Event acara){
        Date compareDate = acara.getStartDate();
        /* For Ascending order*/
        return this.startDate.compareTo(compareDate);
    }

}
