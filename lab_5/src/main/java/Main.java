import java.util.Scanner;

public class Main{
	public static void main (String[] args){
		//Membuat input
		Scanner input = new Scanner (System.in);
		
		//Menginisiasi array dua dimensi yang bernama numbers dengan isi 5 x 5
		Number[][] numbers = new Number[5][5];
		
		//Menginisiasi array numberStates dengan panjang 100
		Number[] numberStates = new Number[100];
		
		//Looping untuk memberi input angka sebanyak 25 angka
		for (int i = 0; i < 5; i++){
			for (int j = 0; j < 5; j++){
				int angka = input.nextInt();
				Number num = new Number(angka,i,j);	//Membuat objek num dari kelas Number
				numbers[i][j] = num;				//Memberi reference pada angka yang terletak dalam array numbers		
				numberStates[angka] = num;			//Memberi reference pada angka yang terletak dalam array numberStates										  
			}	
		}
		
		//Menginisiasi permainan Bingo 
		BingoCard play = new BingoCard(numbers,numberStates);
		
		//Memainkan permainan
		while (true){
			String[] perintah = input.nextLine().split(" ");
			if (perintah[0].equals("MARK")){
				int mark = Integer.parseInt(perintah[1]);
				System.out.println(play.markNum(mark));		//Mark number
			}
			
			else if (perintah[0].equals("INFO")){
				System.out.println(play.info());
			}
			else if (perintah[0].equals("RESTART")){
				System.out.println(play.restart());
			}
			else if (perintah[0].equals("STOP")){
				break;
			}
		}
	}
}
